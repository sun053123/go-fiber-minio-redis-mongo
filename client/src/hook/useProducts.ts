import { useQuery } from 'react-query'
import axios from 'axios'
import { PRODUCTS_API } from '../API'
import { ProductListI } from '../types'

const category = ['all', 'clothes', 'vehicles', 'toys']
const page = 2

const getProducts = async () => {
    console.log('fecthing products')
    const response = await axios.get(`${PRODUCTS_API}?category=${category[2]}`)
    return response.data
}

export const useFetchProducts = () => {
    return useQuery<ProductListI, Error>('products' + category[3] + page, getProducts, {
        refetchOnWindowFocus: false,
        //keep data in cache for 10 sec 
        staleTime: 10000,
        select: (data: ProductListI) => {
            return data
        }
    })
}
