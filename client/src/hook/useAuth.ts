import axios from 'axios'
import { useMutation, useQuery } from 'react-query'
import { ProductI, UserAuthI } from '../types'
import { useAppDispatch } from '../store/store'
import { setUser } from '../store/slices/authSlice'

//custom hook react-query mutation that return data

const getMe = async () => {
    return await axios.get<UserAuthI>('http://localhost:3000/api/auth/me', {
        withCredentials: true
    })
}

export const useSignin = () => {

    const dispatch = useAppDispatch()

    return useMutation<UserAuthI, Error, Omit<UserAuthI, 'id' | 'address' | 'age'>>('signin', async ({
        email,
        password,
    }) => {
        const response = await axios.post('/auth', {
            email: email,
            password: password
        })
        return response.data
    }
    , {
        onSuccess: () => {
            //call getme to get user data then set user in authSlice
            getMe().then(data => {
                dispatch(setUser(data.data))
            })
        }
        , onError: (error) => {
            console.log(error)
        }
    })
}

//custom hook react-query query that return data
export const useProducts = () => {
    return useQuery<ProductI[], Error>('products', async () => {
        const response = await axios.get('/products')
        return response.data
    }, {
        refetchOnWindowFocus: false,
        //keep data in cache for 10 sec 
        staleTime: 10000,
        onSuccess: (data: ProductI[]) => {
            console.log(data)
        }
    })
}

//custom hook react-query query that return data
export const useProduct = (id: string) => {
    return useQuery<ProductI, Error>('product', async () => {
        const response = await axios.get(`/products/${id}`)
        return response.data
    }, {
        refetchOnWindowFocus: false,
        //keep data in cache for 10 sec 
        staleTime: 10000,
        onSuccess: (data: ProductI) => {
            console.log(data)
        }
    })
}