import * as yup from 'yup';

//min6 and max50
const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{6,50}$/;

export const newUserSchema = yup.object().shape({
    email: yup.string().email().required("required"),
    password: yup.string().min(5).max(50).matches(passwordRegex).required("required"),
    confirmPassword: yup.string().oneOf([yup.ref('password'), null], 'Passwords must match').required("required"),
    firstName: yup.string().required("required"),
    lastName: yup.string().required("required"),
    age: yup.number().required("required"),
});
