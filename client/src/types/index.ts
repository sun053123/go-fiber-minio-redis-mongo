
export type ProductI = {
    id: string;
    name: string;
    description: string;
    price: number;
    quantity: number;
    category: string;
    image: string;
    created_at: string;
    updated_at: string;
}

export type PaginationI = {
    count: number;
    limit: number;
    next_page: number;
    page: number;
    prev_page: number;
    total_pages: number;
}

export type ProductListI = {
    products: ProductI[];
    pagination: PaginationI;
    status: number;
}

export type CartItemI = ProductI & { quantity: number };


export type UserAuthI = {
    id: string;
    email: string;
    password: string;
    address: string;
    age: number;
}

//login register return from auth