import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { CartItemI } from '../../types/index';

type CartStateI = {
    items: CartItemI[];
    totalItems: number;
};

const CartLocalStorage = 'cart'


// const initialState: CartStateI = localStorage.getItem(CartLocalStorage) ? JSON.parse(localStorage.getItem(CartLocalStorage) as string) : [];

// Initial state is CartStateI and TotalPrice

const initialState: CartStateI = {
    items: localStorage.getItem(CartLocalStorage) ? JSON.parse(localStorage.getItem(CartLocalStorage) as string).items : [],
    totalItems: localStorage.getItem(CartLocalStorage) ? JSON.parse(localStorage.getItem(CartLocalStorage) as string).items.reduce((acc: any, item: { quantity: any; }) => acc + item.quantity, 0) : 0
};

const cartSlice = createSlice({
    name: 'cart',
    initialState,
    reducers: {
        addToCart: (state, action: PayloadAction<CartItemI>) => {
            //check if product already in cart
            const item = action.payload;
            //find product in cart by id 

            const itemsInCart = state.items.find(itemInCart => itemInCart.id === item.id);
            //if product already in cart, increase quantity
            if (itemsInCart) {
                itemsInCart.quantity += 1;
            } else {
                //if product not in cart, add it
                item.quantity = 1;
                state.items.push(item);
            }

            //update total price
            state.totalItems += 1;

            //save cart in local storage
            localStorage.setItem(CartLocalStorage, JSON.stringify({...state, items: state.items, totalItems: state.totalItems}));
        }
        , removeFromCart: (state, action: PayloadAction<CartItemI>) => {
            const item = action.payload;
            const itemsInCart = state.items.find(itemInCart => itemInCart.id === item.id);
            if (itemsInCart) {
                itemsInCart.quantity -= 1;
                if (itemsInCart.quantity === 0) {
                    state.items.splice(state.items.indexOf(itemsInCart), 1);
                }
            }

            state.totalItems -= 1;
            localStorage.setItem(CartLocalStorage, JSON.stringify({...state, items: state.items, totalItems: state.totalItems}));
        }
        , clearCart: (state) => {
            //delete all items in cart

            state.items.splice(0, state.items.length);

            state.items = [];
            state.totalItems = 0;
            localStorage.setItem(CartLocalStorage, JSON.stringify({...state, items: state.items, totalItems: state.totalItems}));
        }

    }
})

//Action creators
export const { addToCart, removeFromCart, clearCart } = cartSlice.actions;

//Reducer
export default cartSlice.reducer;