import { createSlice, PayloadAction } from '@reduxjs/toolkit'

import { UserAuthI } from '../../types'

type AuthState = {
    user: UserAuthI | null;
}

const initialState: AuthState = {
    user: null
}

const authSlice = createSlice({
    name: 'auth',
    initialState,
    reducers: {
        setUser: (state, action: PayloadAction<UserAuthI>) => {
            state.user = action.payload;
        }
        , clearUser: (state) => {
            state.user = null;
        }
    }
});

export const { setUser, clearUser } = authSlice.actions;

export default authSlice.reducer;