import React from "react";

import { Routes, Route } from "react-router-dom";
import { Box, Button, createTheme, CssBaseline, Stack, styled, ThemeProvider, Typography } from "@mui/material";
import Header from "./components/layouts/Header";
import LoginPage from "./components/pages/LoginPage";
import Menu from "./components/layouts/Menu";
import { blue, orange } from "@mui/material/colors";
import RegisterPage from "./components/pages/RegisterPage";
import StockPage from "./components/pages/StockPage";
import StockCreatePage from "./components/pages/StockCreatePage";
import StockEditPage from "./components/pages/StockEditPage";
import ReportPage from "./components/pages/ReportPage";

export default function App() {
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <ThemeProvider theme={theme}>
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <Header open={open} onDrawerOpen={handleDrawerOpen} />
      <Menu open={open} onDrawerClose={handleDrawerClose} />
      <Main open={open}>
      <DrawerHeader />
      <Routes>
        <Route path="/login" element={<LoginPage />} />
        <Route path="/register" element={<RegisterPage />} />
        <Route path="/report" element={<ReportPage />} />
        <Route path="/stock" element={<StockPage />} />
        <Route path="/stock/create" element={<StockCreatePage />} />
        <Route path="/stock/edit/:id" element={<StockEditPage />} />
        <Route path="*" element={<NotFound />} />
      </Routes>
      </Main>
    </Box>
    </ThemeProvider>
  );
}

const NotFound = () => (
  <div className="d-flex justify-content-center">
    <Typography variant="h3" className="text-danger text-cyan-300">404</Typography>
  </div>
);

const theme = createTheme({
  components: {
    MuiDrawer: {
      styleOverrides: {
        paper: {
          
        },
      },
    },
  },
  typography: {
    fontFamily: "Roboto, sans-serif",
    fontWeightLight: 300,
    fontWeightRegular: 400,
    fontWeightMedium: 500,
    fontWeightBold: 600,
  },
  spacing: 8,
  palette: {
    primary: blue,
    secondary: orange,
    background: {
      default: "#CFD2D6",
    },
  },
});

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
  justifyContent: "flex-end",
}));

const drawerWidth = 240;

const Main = styled("main", { shouldForwardProp: (prop) => prop !== "open" })<{
  open?: boolean;
}>(({ theme, open }) => ({
  flexGrow: 1,
  padding: theme.spacing(3),
  transition: theme.transitions.create("margin", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  marginLeft: `-${drawerWidth}px`,
  ...(open && {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen,
    }),
    marginLeft: 0,
  }),
}));
