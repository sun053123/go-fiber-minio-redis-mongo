import { Box, Container, Typography, Avatar, TextField, FormControlLabel, Button, Checkbox, Grid, Link } from '@mui/material'
// import { Link } from 'react-router-dom'
import LockOutlinedIcon from '@mui/icons-material/LockOutlined'

import React from 'react'

function RegisterPage() {

  const handleSubmitRegister = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault()
  }

  return (
   <Container className='flex flex-col items-center justify-center h-full bg-slate-50 shadow-lg border-[0.5px] border-slate-300'>
       <Box className='mt-16 flex flex-col items-center justify-center'>
          <Avatar className='m-4'>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5" className='font-bold text-violet-900'>
            Sign in
          </Typography>
          <Box component="form" onSubmit={handleSubmitRegister} noValidate className='m-4'>
            <TextField
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
            />
            <TextField
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <Box className='mt-6 mb-4'>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
            >
              Sign In
            </Button>
            </Box>
            <Grid container>
              <Grid item xs>
                <Link href="#" variant="body2">
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                <Link href="#" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
          </Box>
        </Box>
      
   </ Container>
  )
}

export default RegisterPage

// display: 'flex',
//     flexDirection: 'column',
//     alignItems: 'center',
//     justifyContent: 'center',
//     minHeight: '100vh',
//     backgroundColor: '#fafafa',