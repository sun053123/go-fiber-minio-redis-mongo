import React from 'react'

import _ from 'lodash'

import { useFetchProducts } from '../../../hook/useProducts'
import { useAppDispatch, useAppSelector } from '../../../store/store'
import { addToCart, clearCart} from '../../../store/slices/cartSlice'

function StockPage() {

  const { data: products, isLoading, isError, isSuccess,} = useFetchProducts()
  const { totalItems } = useAppSelector(state => state.cart)
  const { user } = useAppSelector(state => state.auth)

  //get cart quantity from store

  //FIXME: add only productid into cart 
  
  console.log(totalItems)

  const dispatch = useAppDispatch()

  if (isLoading) {
    return <div>Loading...</div>
  }

  if (isError) {
    return <div>Error</div>
  }

  if (isSuccess && _.has(products, 'products')) {
    return (
      <>
        <h1>Stock Page</h1>
        <h4>Item in cart </h4>
        <h4>{totalItems}</h4>
        <button onClick={() => dispatch(clearCart())}>Clear Cart</button>
        {products && <ul>
          {products.products.map(product => (
            <li key={product.id}>
              {product.name}
              <button onClick={() => {
                dispatch(addToCart({...product, quantity: 1}))
              }}>
                add to cart
              </button>
            </li>
          ))}
        </ul>}
      </>
    )
  }

return null
}


export default StockPage