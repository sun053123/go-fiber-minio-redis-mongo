module github.com/sun053123/go-paginate

go 1.13

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/go-ini/ini v1.66.6 // indirect
	github.com/go-playground/validator/v10 v10.11.0 // indirect
	github.com/go-redis/redis v6.15.9+incompatible
	github.com/go-redis/redis/v8 v8.11.5
	github.com/gofiber/fiber v1.14.6
	github.com/gofiber/fiber/v2 v2.34.1
	github.com/google/uuid v1.3.0 // indirect
	github.com/jinzhu/gorm v1.9.16 // indirect
	github.com/joho/godotenv v1.4.0
	github.com/minio/minio-go v6.0.14+incompatible
	github.com/minio/minio-go/v7 v7.0.28
	github.com/pkg/errors v0.9.1 // indirect
	go.mongodb.org/mongo-driver v1.9.1
	golang.org/x/crypto v0.0.0-20220525230936-793ad666bf5e // indirect
	golang.org/x/image v0.0.0-20220617043117-41969df76e82 // indirect
	gorm.io/driver/postgres v1.3.7 // indirect
	gorm.io/gorm v1.23.6 // indirect
)
