package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/joho/godotenv"

	"github.com/sun053123/go-paginate/database"
	"github.com/sun053123/go-paginate/routers"
)

func main() {

	PORT := os.Getenv("PORT")

	//get redis client
	redisClient := database.GetRedisClient()

	//get minio client
	minioClient, errMinio := database.GetMinioClient()
	if errMinio != nil {
		panic(errMinio)
	} else {
		fmt.Println("Minio ===> Connected to Minio")
	}

	//get database connection from database
	db, errDB := database.GetMongoClient()
	defer db.Disconnect(context.TODO())
	if errDB != nil {
		panic(errDB)
	} else {
		fmt.Println("MongoDB ===> Connected to MongoDB")
	}

	//start server
	app := fiber.New()

	//CORS
	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowHeaders: "Origin, Content-Type, Accept",
	}))

	// app.Use(csrf.New(csrf.Config{
	// 	KeyLookup:      "header:X-Csrf-Token",
	// 	CookieName:     "csrf_",
	// 	CookieSameSite: "Strict",
	// 	Expiration:     1 * time.Hour,
	// 	KeyGenerator:   utils.UUID,
	// }))

	// app.Use(limiter.New())

	//cdn dst image
	app.Static("/images", "./images")

	routers.ProductRoutes(app, db, redisClient, minioClient)
	routers.AuthRoutes(app, db)

	//check health
	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hello, World!")
	})

	fmt.Printf("server ready at http://localhost:%s", PORT)
	app.Listen("192.168.1.23:" + PORT)

}

func init() {
	//init timezone
	loc, err := time.LoadLocation("Asia/Bangkok")
	if err != nil {
		panic(err)
	}
	time.Local = loc

	//init env
	err = godotenv.Load(".env")
	if err != nil {
		log.Fatal("Error loading .env file")
	}
	fmt.Println(".env ===> Loaded Successfully")

}
