package errs

import (
	"net/http"
)

type AppError struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
	Type    string `json:"type"`
}

func (err AppError) Error() string {
	return err.Message
}

func NewNotFoundError(msg string) error {
	return AppError{
		Message: msg,
		Code:    http.StatusNotFound,
	}
}

func NewUnexpectedError() error {
	return AppError{
		Message: "Unexpected error",
		Code:    http.StatusInternalServerError,
	}
}

func NewBadRequestError(msg string) error {
	return AppError{
		Message: msg,
		Code:    http.StatusBadRequest,
	}
}

func NewForbiddenError(msg string) error {
	return AppError{
		Message: msg,
		Code:    http.StatusForbidden,
	}
}

func NewUnauthorizedError(msg string) error {
	return AppError{
		Message: msg,
		Code:    http.StatusUnauthorized,
	}
}
