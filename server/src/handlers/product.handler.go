package handlers

import (
	"errors"
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/sun053123/go-paginate/errs"
	"github.com/sun053123/go-paginate/src/services"
	"github.com/sun053123/go-paginate/utils"
)

type productHandler struct {
	productSrv services.ProductService
}

func NewProductHandler(productSrv services.ProductService) ProductHandler {
	return productHandler{
		productSrv: productSrv,
	}
}

var Validator = validator.New()

func (handl productHandler) GetAllProducts(c *fiber.Ctx) error {
	/////////////////////////////////////////////////Validate request///////////////////////////////////////////////////////////////
	category := c.Query("category")
	name := c.Query("name")
	page := c.Query("page")

	//set null page and category
	if page == "" {
		page = "1"
	}

	//if category === all get all products
	if category == "all" {
		category = ""
	}

	//parse page to int64
	pageInt, errInt := strconv.ParseInt(page, 10, 64)
	if errInt != nil {
		return handleError(c, errors.New("page must be a number"))
	}

	//if pageint < 1 return error
	if pageInt < 1 {
		return handleError(c, errors.New("page must be greater than 0"))
	}

	var products *services.DataResponse
	var err error

	//if name == "" use get all products
	if name == "" {
		fmt.Println("Get all products")
		products, err = handl.productSrv.GetAllProducts(category, pageInt)
	} else {
		fmt.Println("Get product by name")
		products, err = handl.productSrv.GetProductByName(name, category, pageInt)
	}

	if err != nil {
		handleError(c, err)
	}

	return c.Status(http.StatusAccepted).JSON(fiber.Map{
		"status":   http.StatusAccepted,
		"products": products.ProductResponse,
		"paginate": products.Pagination,
	})
}

func (handl productHandler) GetProductByID(c *fiber.Ctx) error {
	id := c.Params("id")
	product, err := handl.productSrv.GetProductByID(id)
	if err != nil {
		return handleError(c, err)
	}

	return c.Status(http.StatusAccepted).JSON(fiber.Map{
		"status":  http.StatusAccepted,
		"product": product,
	})
}

func (handl productHandler) CreateProduct(c *fiber.Ctx) error {
	//get userinfo from middleware with claims type struct
	userinfo := c.Locals("userinfo").(*utils.Claims)

	//get body from request
	var productRequest services.NewProductRequest
	if err := c.BodyParser(&productRequest); err != nil {
		return handleError(c, err)
	}

	//validate request body //like express-validator
	var errors []*errs.AppError
	err := Validator.Struct(productRequest)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			var apperr errs.AppError
			apperr.Message = fmt.Sprintf("need %s %s in %s tag", err.Param(), err.Tag(), err.Field())
			apperr.Code = http.StatusBadRequest
			errors = append(errors, &apperr)
		}
		return c.Status(http.StatusBadRequest).JSON(errors)
	}

	// //get c.Locals("uploadedImage") from middleware minio (uploaded image)
	imageInfo := c.Locals("uploadedImage")
	//add imagepath to productRequest
	productRequest.Image = imageInfo.(string)

	//send with productResponse Struct to createProduct
	response, err := handl.productSrv.CreateProduct(productRequest, *userinfo)
	if err != nil {
		return handleError(c, err)
	}

	return c.Status(http.StatusAccepted).JSON(fiber.Map{
		"status":  http.StatusAccepted,
		"product": response,
	})
}

func (handl productHandler) UpdateProduct(c *fiber.Ctx) error {

	//get id from request
	id := c.Params("id")

	//get userinfo from middleware
	userinfo := c.Locals("userinfo").(*utils.Claims)

	var productRequest services.NewProductRequest

	if err := c.BodyParser(&productRequest); err != nil {
		return handleError(c, err)
	}

	//validate request body //like express-validator
	var errors []*errs.AppError
	err := Validator.Struct(productRequest)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			var apperr errs.AppError
			apperr.Message = fmt.Sprintf("need %s %s in %s tag", err.Param(), err.Tag(), err.Field())
			apperr.Code = http.StatusBadRequest
			errors = append(errors, &apperr)
		}
		return c.Status(http.StatusBadRequest).JSON(errors)
	}

	//get c.Locals("uploadedImage") from middleware minio (uploaded image)
	//if body image request is string (no need to update image path)

	file, _ := c.FormFile("image")
	//add imagepath to productRequest if we have new image
	if file != nil {
		imageInfo := c.Locals("uploadedImage")
		productRequest.Image = imageInfo.(string)
	}

	//send with productResponse Struct to createProduct
	response, err := handl.productSrv.UpdateProduct(productRequest, *userinfo, id)
	if err != nil {
		return handleError(c, err)
	}

	return c.Status(http.StatusAccepted).JSON(fiber.Map{
		"status":  http.StatusAccepted,
		"product": response,
	})
}

func (handl productHandler) DeleteProduct(c *fiber.Ctx) error {

	id := c.Params("id")

	//get userinfo from middleware
	userinfo := c.Locals("userinfo").(*utils.Claims)

	err := handl.productSrv.DeleteProduct(*userinfo, id)
	if err != nil {
		return handleError(c, err)
	}

	return c.Status(http.StatusAccepted).JSON(fiber.Map{
		"status":  http.StatusAccepted,
		"message": "Product deleted",
	})
}

func (handle productHandler) SearchProduct(c *fiber.Ctx) error {

	search := c.Query("search")
	//search trim whitespace, space, tab, newline
	search = strings.TrimSpace(search)
	search = strings.Replace(search, " ", "", -1)

	response, err := handle.productSrv.SearchProduct(search)
	if err != nil {
		return handleError(c, err)
	}

	return c.Status(http.StatusAccepted).JSON(fiber.Map{
		"status":  http.StatusAccepted,
		"product": response,
	})

}

func (handl productHandler) BuyProduct(c *fiber.Ctx) error {

	return c.Status(http.StatusAccepted).JSON(fiber.Map{
		"status":  http.StatusAccepted,
		"message": "Product bought",
	})
}
