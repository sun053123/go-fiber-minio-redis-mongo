package handlers

import (
	"fmt"
	"net/http"
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"github.com/sun053123/go-paginate/errs"
	"github.com/sun053123/go-paginate/src/services"
	"github.com/sun053123/go-paginate/utils"
)

type authHandler struct {
	authSrv services.AuthService
}

func NewAuthHandler(authSrv services.AuthService) AuthHandler {
	return authHandler{
		authSrv: authSrv,
	}
}

func (serv authHandler) GetMe(c *fiber.Ctx) error {

	//get userinfo from local
	userinfo := c.Locals("userinfo").(*utils.Claims)

	//call service
	user, err := serv.authSrv.GetMe(userinfo.ID)
	if err != nil {
		return handleError(c, err)
	}

	return c.Status(http.StatusAccepted).JSON(fiber.Map{
		"status":  http.StatusOK,
		"message": user,
	})
}

func (serv authHandler) Login(c *fiber.Ctx) error {

	//parse body to struct
	var loginreq services.LoginRequest
	err := c.BodyParser(&loginreq)
	if err != nil {
		return err
	}
	//validate request body //like express-validator
	var errors []*errs.AppError
	err = Validator.Struct(loginreq)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			var apperr errs.AppError
			apperr.Message = fmt.Sprintf("need %s %s in %s tag", err.Param(), err.Tag(), err.Field())
			apperr.Code = http.StatusBadRequest
			errors = append(errors, &apperr)
		}
		return c.Status(http.StatusBadRequest).JSON(errors)
	}

	//call service
	tokenString, err := serv.authSrv.LoginUser(loginreq)
	if err != nil {
		return handleError(c, err)
	}

	// set token to cookie
	cookie := &fiber.Cookie{
		Name:     "jwt-token",
		Value:    tokenString,
		MaxAge:   int(time.Now().Add(time.Hour * 72).Unix()),
		HTTPOnly: true,
	}

	c.Cookie(cookie)

	return c.JSON(fiber.Map{
		"status":  http.StatusOK,
		"message": "login success",
	})

}

func (serv authHandler) Logout(c *fiber.Ctx) error {

	//delete cookie
	cookie := &fiber.Cookie{
		Name:     "jwt-token",
		Value:    "",
		MaxAge:   -1,
		HTTPOnly: true,
	}

	c.Cookie(cookie)

	return c.Status(http.StatusOK).JSON(fiber.Map{
		"status": http.StatusOK,
	})

}

func (serv authHandler) Register(c *fiber.Ctx) error {
	//parse body to struct
	var authreq services.AuthRequest
	err := c.BodyParser(&authreq)
	if err != nil {
		return err
	}

	fmt.Println(authreq)

	//validate request body //like express-validator
	var errors []*errs.AppError
	err = Validator.Struct(authreq)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			var apperr errs.AppError
			apperr.Message = fmt.Sprintf("need %s %s in %s tag", err.Param(), err.Tag(), err.Field())
			apperr.Code = http.StatusBadRequest
			errors = append(errors, &apperr)
		}
		return c.Status(http.StatusBadRequest).JSON(errors)
	}

	//call service
	tokenString, err := serv.authSrv.Register(authreq)
	if err != nil {
		return handleError(c, err)
	}

	// set token to cookie
	cookie := &fiber.Cookie{
		Name:     "jwt-token",
		Value:    tokenString,
		MaxAge:   int(time.Now().Add(time.Hour * 72).Unix()),
		HTTPOnly: true,
	}

	c.Cookie(cookie)

	return c.JSON(fiber.Map{
		"status":  http.StatusOK,
		"message": "register success",
	})
}

func (serv authHandler) UpdateUser(c *fiber.Ctx) error {
	return nil
}

func (serv authHandler) DeleteUser(c *fiber.Ctx) error {
	return nil
}
