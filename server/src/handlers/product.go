package handlers

import (
	"github.com/gofiber/fiber/v2"
)

type ProductHandler interface {
	GetAllProducts(c *fiber.Ctx) error
	GetProductByID(c *fiber.Ctx) error
	CreateProduct(c *fiber.Ctx) error
	UpdateProduct(c *fiber.Ctx) error
	DeleteProduct(c *fiber.Ctx) error
	SearchProduct(c *fiber.Ctx) error
	BuyProduct(c *fiber.Ctx) error
}
