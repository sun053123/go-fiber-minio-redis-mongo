package handlers

import (
	"net/http"

	"github.com/gofiber/fiber/v2"
	"github.com/sun053123/go-paginate/errs"
)

func handleError(c *fiber.Ctx, err error) error {
	switch e := err.(type) {
	case errs.AppError:
		//error from service
		return c.Status(e.Code).JSON(e)
	case error:
		//error from handler
		return c.Status(http.StatusInternalServerError).JSON(errs.AppError{
			Message: e.Error(),
			Code:    http.StatusInternalServerError,
		})
	}
	return c.Status(http.StatusInternalServerError).JSON(errs.AppError{
		Message: "Unexpected error",
		Code:    http.StatusInternalServerError,
	})
}
