package entities

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/minio/minio-go/v7"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type productEntity struct {
	db          *mongo.Client
	redisClient *redis.Client
	minioClient *minio.Client
}

func NewProductEntity(db *mongo.Client, redisClient *redis.Client, minioClient *minio.Client) ProductEntity {
	mockData(db)
	return productEntity{
		db:          db,
		redisClient: redisClient,
		minioClient: minioClient,
	}
}

const LIMIT_PER_PAGE = 20

//create context timeout

func (ent productEntity) FindAllProducts(category string, page int64) (products []product, pagination *Paginate, err error) {
	//create context timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// get from redis
	redisKey := "entity::GetAllProducts" + category + "::" + fmt.Sprint(page)
	redisPaginationKey := "entity::GetAllProducts" + category + "::" + fmt.Sprint(page) + "::pagination"

	productsJson, err := ent.redisClient.Get(context.Background(), redisKey).Result()
	paginationJson, err := ent.redisClient.Get(context.Background(), redisPaginationKey).Result()
	if err == nil {
		err = json.Unmarshal([]byte(productsJson), &products)
		if err == nil {
			//get pagination from redis
			err = json.Unmarshal([]byte(paginationJson), &pagination)
			if err == nil {
				return products, pagination, nil
			}
		}
	}

	// get from mongoDB
	collection := ent.db.Database(DatabaseName).Collection(CollectionProduct)

	//count pagination
	countch := make(chan int64, 1)
	go ent.countRecords(countch, "", category)

	//get pagination
	skip := (page - 1) * LIMIT_PER_PAGE

	var findOptions = options.Find()
	findOptions.SetLimit(LIMIT_PER_PAGE)
	findOptions.SetSkip(skip)
	findOptions.SetSort(bson.M{"created_at": -1})

	//where deletedAt is empty string // and category is in the category
	filter := bson.M{"deleted_at": "", "category": bson.M{"$regex": category}}

	cur, err := collection.Find(ctx,
		filter,
		findOptions)
	if err != nil {
		return nil, nil, err
	}
	defer cur.Close(ctx)

	// loop through the cursor to decode into []product struct
	for cur.Next(ctx) {
		var product product
		err = cur.Decode(&product)
		if err != nil {
			return nil, nil, err
		}
		products = append(products, product)
	}

	//set paginate
	count := <-countch

	totalPage := int64(count / LIMIT_PER_PAGE)
	var nextPage int64
	// if page is last page, next page is total page
	if page > totalPage {
		nextPage = totalPage
	} else {
		nextPage = page + 1
	}
	pagination = &Paginate{
		Page:       page,
		Limit:      LIMIT_PER_PAGE,
		Count:      count,
		PrevPage:   page - 1,
		NextPage:   nextPage,
		TotalPages: totalPage,
	}

	// set to redis
	data, err := json.Marshal(products)
	if err != nil {
		return nil, nil, err
	}
	ent.redisClient.Set(context.Background(), redisKey, string(data), time.Second*10).Err()
	if err != nil {
		return nil, nil, err
	}
	// set pagination to redis
	data, err = json.Marshal(pagination)
	if err != nil {
		return nil, nil, err
	}
	ent.redisClient.Set(context.Background(), redisPaginationKey, string(data), time.Second*10).Err()
	if err != nil {
		return nil, nil, err
	}

	return products, pagination, nil
}

func (ent productEntity) FindProductByID(id string) (prod *product, err error) {
	//create context timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// get from redis
	redisKey := "entity::GetProductByID::" + id
	productJson, err := ent.redisClient.Get(context.Background(), redisKey).Result()
	if err == nil {
		err = json.Unmarshal([]byte(productJson), &prod)
		if err == nil {
			fmt.Println("get product from redis")
			return prod, nil
		}
	}

	//convert id to bson.ObjectId
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	// get from mongoDB
	collection := ent.db.Database(DatabaseName).Collection(CollectionProduct)

	//aggregate product by id and populate user and we don't need password
	pipeline := []bson.M{
		{"$match": bson.M{"_id": oid, "deleted_at": ""}},
		{"$lookup": bson.M{
			"from":         "users",
			"localField":   "_user_id",
			"foreignField": "_id",
			"as":           "user",
		}},
		{"$unwind": "$user"},
	}

	cur, err := collection.Aggregate(ctx, pipeline)
	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)
	//if no product found, return nil

	//get product from cursor
	for cur.Next(ctx) {
		// loop through the cursor to decode into []product struct
		err = cur.Decode(&prod)
		if err != nil {
			return nil, err
		}

	}

	//it can loop through nested cursor to get user *****************
	fmt.Println("get product from mongoDB", prod.User.Email)

	// set to redis
	data, err := json.Marshal(prod)
	if err != nil {
		return nil, err
	}

	ent.redisClient.Set(context.Background(), redisKey, string(data), time.Second*10).Err()
	if err != nil {
		return nil, err
	}

	return prod, nil
}

func (ent productEntity) FindProductByName(name string, category string, page int64) (products []product, pagination *Paginate, err error) {
	//create context timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// get from database
	collection := ent.db.Database(DatabaseName).Collection(CollectionProduct)

	//count pagination
	countch := make(chan int64, 1)
	go ent.countRecords(countch, name, category)

	//get pagination
	skip := (page - 1) * LIMIT_PER_PAGE

	//where deletedAt is empty string and name is in the name
	filter := bson.M{"deleted_at": "", "name": bson.M{"$regex": name}, "category": bson.M{"$regex": category}}

	//filter lookup
	pipeline := []bson.M{
		{"$match": filter},
		{"$lookup": bson.M{
			"from":         "users",
			"localField":   "_user_id",
			"foreignField": "_id",
			"as":           "user",
		}},
		{"$unwind": "$user"},
		{"$skip": skip},
		{"$limit": LIMIT_PER_PAGE},
		{"$sort": bson.M{"created_at": -1}},
	}

	// โค้ดเก่าใช้ populate ไม่ได้
	//sort by created_at
	// var findOptions = options.Find()
	// findOptions.SetLimit(LIMIT_PER_PAGE)
	// findOptions.SetSkip(skip)
	// findOptions.SetSort(bson.M{"created_at": -1})

	// cur, err := collection.Find(ctx, filter, findOptions)
	// if err != nil {
	// 	return nil, nil, err
	// }

	cur, err := collection.Aggregate(ctx, pipeline)
	if err != nil {
		return nil, nil, err
	}

	defer cur.Close(ctx)

	// loop through the cursor to decode into nested []product struct
	for cur.Next(ctx) {
		var p bson.M
		err = cur.Decode(&p)
		if err != nil {
			return nil, nil, err
		}
		products = append(products, product{
			ID:          p["_id"].(primitive.ObjectID),
			Name:        p["name"].(string),
			Description: p["description"].(string),
			//bson is returning int32, but we need int
			Price:     int(p["price"].(int32)),
			Quantity:  int(p["quantity"].(int32)),
			Category:  p["category"].(string),
			CreatedAt: p["created_at"].(string),
			UpdatedAt: p["updated_at"].(string),
			DeletedAt: p["deleted_at"].(string),
			User: &auth{
				ID:    p["user"].(bson.M)["_id"].(primitive.ObjectID),
				Email: p["user"].(bson.M)["email"].(string),
			},
		})
	}

	fmt.Println("get product from mongoDB", products)

	//count pagination
	count := <-countch
	pagination = &Paginate{
		Page:       page,
		Limit:      LIMIT_PER_PAGE,
		Count:      count,
		PrevPage:   page - 1,
		NextPage:   page + 1,
		TotalPages: int64(count/LIMIT_PER_PAGE) + 1,
	}

	return products, pagination, nil
}

func (ent productEntity) CreateProduct(user string, email string, name string, description string, price int, quantity int, category string, imagepath string) (products *product, err error) {
	//create context timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//convert string to bson.ObjectId
	userId, err := primitive.ObjectIDFromHex(user)
	if err != nil {
		return nil, err
	}

	if err != nil {
		return nil, err
	}

	// create new product with bson.NewObjectId()
	products = &product{
		ID:          primitive.NewObjectID(),
		UserID:      userId,
		Name:        name,
		Description: description,
		Price:       price,
		Quantity:    quantity,
		Category:    category,
		Image:       imagepath,
		CreatedAt:   time.Now().Format("2006-01-02 15:04:05"),
		UpdatedAt:   time.Now().Format("2006-01-02 15:04:05"),
	}

	// insert to mongoDB
	collection := ent.db.Database(DatabaseName).Collection(CollectionProduct)
	_, err = collection.InsertOne(ctx, products)
	if err != nil {
		return nil, err
	}

	// set to redis //add user email
	data, err := json.Marshal(
		map[string]interface{}{
			"id":          products.ID.Hex(),
			"user_id":     products.UserID.Hex(),
			"name":        products.Name,
			"description": products.Description,
			"price":       products.Price,
			"quantity":    products.Quantity,
			"category":    products.Category,
			"image":       products.Image,
			"created_at":  products.CreatedAt,
			"updated_at":  products.UpdatedAt,
			"deleted_at":  products.DeletedAt,
			"user": map[string]interface{}{
				"id":    products.UserID.Hex(),
				"email": email,
			},
		})

	if err != nil {
		return nil, err
	}
	ent.redisClient.Set(ctx, "entity::GetProductByID::"+products.ID.Hex(), string(data), time.Second*10).Err()
	if err != nil {
		return nil, err
	}

	return products, nil
}

func (ent productEntity) UpdateProduct(name string, description string, price int, quantity int, category string, image string, id string) (updatedproduct *product, err error) {
	//create context timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//convert id to bson.ObjectId
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	// update to mongoDB with id
	collection := ent.db.Database(DatabaseName).Collection(CollectionProduct)

	//update by id
	filter := bson.M{"_id": oid}
	update := bson.M{
		"$set": bson.M{
			"name":        name,
			"description": description,
			"price":       price,
			"quantity":    quantity,
			"category":    category,
			"image":       image,
			"updated_at":  time.Now().Format("2006-01-02 15:04:05"),
		},
	}

	//set return updated product
	upsert := true
	after := options.After
	options := &options.FindOneAndUpdateOptions{ReturnDocument: &after, Upsert: &upsert}

	//find by id and update
	err = collection.FindOneAndUpdate(ctx, filter, update, options).Decode(&updatedproduct)
	if err != nil {
		return nil, err
	}

	// set updated to redis
	data, err := json.Marshal(updatedproduct)
	if err != nil {
		return nil, err
	}

	ent.redisClient.Set(context.Background(), "entity::GetProductByID::"+updatedproduct.ID.Hex(), string(data), time.Second*10).Err()
	if err != nil {
		return nil, err
	}
	return updatedproduct, nil
}

func (ent productEntity) DeleteProduct(id string) (isDeleted bool, err error) {
	//create context timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//convert id to bson.ObjectId
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return false, err
	}

	// update deletedAt flag to current time
	collection := ent.db.Database(DatabaseName).Collection(CollectionProduct)

	//find by id and update
	updated, err := collection.UpdateOne(ctx, bson.M{"_id": oid}, bson.M{"$set": bson.M{"deleted_at": time.Now().Format("2006-01-02 15:04:05")}})
	if err != nil {
		return false, err
	}

	//if updated, return product
	if updated.ModifiedCount > 0 {
		isDeleted = true
	}

	// delete in redis
	ent.redisClient.Del(context.Background(), "entity::GetProductByID::"+id).Err()
	if err != nil {
		return false, err
	}

	return isDeleted, nil
}

func (ent productEntity) SearchProduct(search string) (products []product, err error) {
	//create context timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	// search by name, description and category
	collection := ent.db.Database(DatabaseName).Collection(CollectionProduct)

	/////////////////////////////////////////////////////////////////////////////////////////////
	// //find search in name, description and category only one of them
	// filter := bson.M{"deleted_at": "", "$or": []bson.M{
	// 	{"name": bson.M{"$regex": search, "$options": "i"}},
	// 	{"description": bson.M{"$regex": search, "$options": "i"}},
	// 	{"category": bson.M{"$regex": search, "$options": "i"}},
	// }}

	// //options
	// findOptions := options.Find()
	// findOptions.SetLimit(50)
	// // findOptions.SetSort(bson.M{"created_at": -1})
	// //sort by less of string length
	// // findOptions.SetSort(bson.M{"length(name)": -1})

	// //sory by nearest to the search $or
	// findOptions.SetSort(bson.M{"length(name)": 1})

	// //find
	// cur, err := collection.Find(ctx, filter, findOptions)
	// if err != nil {
	// 	return nil, err
	// }

	// //defer for closing cursor avoid memory leak
	// defer cur.Close(ctx)

	// // loop through the cursor to decode into []product struct
	// for cur.Next(ctx) {
	// 	var product product
	// 	err = cur.Decode(&product)
	// 	if err != nil {
	// 		return nil, err
	// 	}
	// 	products = append(products, product)
	// }
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	filter := bson.M{"deleted_at": "", "$or": []bson.M{
		{"name": bson.M{"$regex": primitive.Regex{
			Pattern: search,
			Options: "i",
		}}},
		{"description": bson.M{"$regex": primitive.Regex{
			Pattern: search,
			Options: "i",
		}}},
		{"category": bson.M{"$regex": primitive.Regex{
			Pattern: search,
			Options: "i",
		}}},
	}}

	//aggregate options
	aggregateOptions := options.Aggregate()
	aggregateOptions.SetAllowDiskUse(true)
	aggregateOptions.SetMaxTime(10 * time.Second)
	//query with speed up pipeline

	//aggregate by equal to search
	pipeline := []bson.M{
		{"$match": filter},
		{"$limit": 50},
	}

	//find by pipeline
	cur, err := collection.Aggregate(ctx, pipeline, aggregateOptions)
	if err != nil {
		return nil, err
	}

	products = make([]product, 0)
	for cur.Next(ctx) {
		var product product
		err = cur.Decode(&product)
		if err != nil {
			return nil, err
		}
		products = append(products, product)
	}

	return products, nil
}

func (ent productEntity) countRecords(countch chan int64, name string, category string) {
	//create context timeout
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	collection := ent.db.Database(DatabaseName).Collection(CollectionProduct)
	count, err := collection.CountDocuments(ctx, bson.M{"deleted_at": "", "name": bson.M{"$regex": name}, "category": bson.M{"$regex": category}})
	if err != nil {
		countch <- 0
	}
	countch <- count
}
