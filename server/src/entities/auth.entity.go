package entities

import (
	"context"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type authEntity struct {
	db *mongo.Client
}

func NewAuthEntity(db *mongo.Client) AuthEntity {
	return &authEntity{db: db}
}

func (ent authEntity) GetUserInfo(id string) (user *auth, err error) {

	//create context
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//collection
	collection := ent.db.Database(DatabaseName).Collection(CollectionUser)

	//convert id to bson.ObjectId
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	// filter
	filter := bson.M{"_id": oid}

	// find user
	err = collection.FindOne(ctx, filter).Decode(&user)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (ent authEntity) GetUserInfoByEmail(email string) (user *auth, err error) {
	//create context
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//collection
	collection := ent.db.Database(DatabaseName).Collection(CollectionUser)

	// filter
	filter := bson.M{"email": email}

	// find user
	err = collection.FindOne(ctx, filter).Decode(&user)
	if err != nil {
		return nil, err
	}

	return user, nil
}

func (ent authEntity) CreateUser(email string, password string, role string, avatar string) (newUser *auth, err error) {

	// create user
	newUser = &auth{
		ID:        primitive.NewObjectID(),
		Email:     email,
		Password:  password,
		Role:      role,
		Avatar:    avatar,
		CreatedAt: time.Now().Format("2006-01-02 15:04:05"),
		UpdatedAt: time.Now().Format("2006-01-02 15:04:05"),
		DeletedAt: "",
	}

	//collection
	collection := ent.db.Database(DatabaseName).Collection(CollectionUser)

	// insert user
	_, err = collection.InsertOne(context.TODO(), newUser)
	if err != nil {
		return nil, err
	}

	return newUser, nil
}

func (ent authEntity) UpdateUser(email string, role string, avatar string, id string) (updatedUser *auth, err error) {

	//create context
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//collection
	collection := ent.db.Database(DatabaseName).Collection(CollectionUser)

	//convert id to ObjectID
	//convert id to bson.ObjectId
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, err
	}

	// filter
	filter := bson.M{"_id": oid}

	// update user
	update := bson.M{
		"$set": bson.M{
			"email":      email,
			"role":       role,
			"avatar":     avatar,
			"updated_at": time.Now().Format("2006-01-02 15:04:05"),
		},
	}

	//set return updated user
	upsert := true
	after := options.After
	options := &options.FindOneAndUpdateOptions{ReturnDocument: &after, Upsert: &upsert}

	//find by id and update
	err = collection.FindOneAndUpdate(ctx, filter, update, options).Decode(&updatedUser)
	if err != nil {
		return nil, err
	}

	return updatedUser, nil
}

func (ent authEntity) DeleteUser(id string) (bool, error) {
	//create context
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	//collection
	collection := ent.db.Database(DatabaseName).Collection(CollectionUser)

	//convert id to bson.ObjectId
	oid, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return false, err
	}

	// filter
	filter := bson.M{"_id": oid}

	// delete user
	_, err = collection.DeleteOne(ctx, filter)
	if err != nil {
		return false, err
	}

	return true, nil
}
