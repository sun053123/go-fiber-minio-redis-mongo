package entities

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
)

// Product Schema จะไม่ถูก Exposed ให้กับผู้ใช้งาน
type product struct {
	ID     primitive.ObjectID `bson:"_id"`
	UserID primitive.ObjectID `bson:"_user_id"`
	//we do not store User user in product collection //use only for query (populate)
	User        *auth  `bson:",omitempty"`
	Name        string `bson:"name"`
	Description string `bson:"description"`
	Price       int    `bson:"price"`
	Quantity    int    `bson:"quantity"`
	Category    string `bson:"category"`
	Image       string `bson:"image"`
	CreatedAt   string `bson:"created_at"`
	UpdatedAt   string `bson:"updated_at"`
	DeletedAt   string `bson:"deleted_at"`
}

//ไม่ใช้แล้ว ไปใช้ auth struct แทน
// type user struct {
// 	ID    primitive.ObjectID `bson:"_id"`
// 	Email string             `bson:"email,omitempty"`
// }

type Paginate struct {
	Page       int64 `json:"page"`
	Limit      int64 `json:"limit"`
	Count      int64 `json:"count"`
	PrevPage   int64 `json:"prev_page"`
	NextPage   int64 `json:"next_page"`
	TotalPages int64 `json:"total_pages"`
}

// ProductEntity จะถูก Exposed ให้กับผู้ใช้งาน
type ProductEntity interface {
	FindAllProducts(category string, page int64) ([]product, *Paginate, error)
	FindProductByID(id string) (*product, error)
	FindProductByName(name string, category string, page int64) ([]product, *Paginate, error)
	CreateProduct(user string, email string, name string, description string, price int, quantity int, category string, image string) (*product, error)
	UpdateProduct(name string, description string, price int, quantity int, category string, image string, id string) (*product, error)
	SearchProduct(search string) ([]product, error)
	DeleteProduct(id string) (bool, error)
}

// productEntity จะถูกสร้างขึ้นมาจาก NewProductEntity
func mockData(db *mongo.Client) error {
	ctx, cancel := context.WithTimeout(context.Background(), 500*time.Second)
	defer cancel()

	var collection = db.Database(DatabaseName).Collection(CollectionProduct)

	//skip if data is already exist
	filter := bson.M{"created_at": bson.M{"$exists": true}}
	var count, err = collection.CountDocuments(ctx, filter)
	if err != nil {
		return err
	}
	// ถ้ามีข้อมูลแล้ว จะข้ามการ mock data
	if count > 0 {
		fmt.Println("mock data already exist")
		return nil
	}

	////////////////////////////////////////////////////////////////////////////

	//random data 1 - 100
	var randomMock = func() int {
		return rand.Intn(100)
	}

	//random category []
	var randomCategory = func() string {
		var categories = []string{"foods", "vehicles", "electronics", "toys", "clothes", "others"}
		return categories[rand.Intn(len(categories))]
	}

	//number of products
	var numberOfProducts = 1000

	//userid gen oid from string
	var userID = func(user string) primitive.ObjectID {
		oid, err := primitive.ObjectIDFromHex(user)
		if err != nil {
			log.Fatal(err)
		}
		return oid
	}

	// create mock data
	fmt.Println("mockdata . . .")
	for i := 0; i < numberOfProducts; i++ {
		var product = product{
			ID:          primitive.NewObjectID(),
			UserID:      userID("62b30ec949f1ae07e68f3307"),
			Name:        "product" + fmt.Sprintf("%d", i),
			Description: "description" + fmt.Sprintf("%d", i),
			Price:       randomMock(),
			Quantity:    randomMock(),
			Category:    randomCategory(),
			CreatedAt:   time.Now().Format("2006-01-02 15:04:05"),
			UpdatedAt:   time.Now().Format("2006-01-02 15:04:05"),
			DeletedAt:   "",
		}
		_, err := collection.InsertOne(ctx, product)
		if err != nil {
			log.Fatal(err)
		}
	}
	return nil
}

// func createIndex(db *mongo.Client) error {
// 	ctx, cancel := context.WithTimeout(context.Background(), 500*time.Second)
// 	defer cancel()

// 	fmt.Println("create index . . .")

// 	var collection = db.Database(DatabaseName).Collection(CollectionProduct)

// 	mod := mongo.IndexModel{
// 		Keys: bson.M{
// 			"name": 1,
// 		},
// 		Options: options.Index().SetUnique(true),
// 	}

// 	_, err := collection.Indexes().CreateOne(ctx, mod)
// 	if err != nil {
// 		fmt.Println(err)
// 		return err
// 	}

// 	return nil

// }
