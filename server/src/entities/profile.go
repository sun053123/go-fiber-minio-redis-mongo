package entities

import "go.mongodb.org/mongo-driver/bson/primitive"

type profile struct {
	ID     primitive.ObjectID `bson:"_id"`
	UserID primitive.ObjectID `bson:"_user_id"`
	User   *auth              `bson:",inline,omitempty"`
	//for find user's product (populate from db)
	Product   []*product `bson:",omitempty"`
	FirstName string     `bson:"first_name"`
	LastName  string     `bson:"last_name"`
	Address   string     `bson:"address"`
	Phone     string     `bson:"phone"`
	CreatedAt string     `bson:"created_at"`
	UpdatedAt string     `bson:"updated_at"`
}

type ProfileEntity interface {
	CreateProfile(user string, firstName string, lastName string, address string, phone string) (*profile, error)
	UpdateProfile(firstName string, lastName string, address string, phone string, id string) (*profile, error)
	GetProfileByUserID(userID string) (*profile, error)
	GetProfileByID(id string) (*profile, error)
}
