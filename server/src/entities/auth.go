package entities

import "go.mongodb.org/mongo-driver/bson/primitive"

type auth struct {
	ID         primitive.ObjectID `bson:"_id"`
	Email      string             `bson:"email"`
	Password   string             `bson:"password"`
	Role       string             `bson:"role"`
	Avatar     string             `bson:"avatar"`
	CreatedAt  string             `bson:"created_at"`
	UpdatedAt  string             `bson:"updated_at"`
	DeletedAt  string             `bson:"deleted_at"`
	IsAccepted bool               `bson:"is_accepted"`
}

type AuthEntity interface {
	GetUserInfo(id string) (*auth, error)
	GetUserInfoByEmail(email string) (*auth, error)
	CreateUser(email string, password string, role string, avatar string) (*auth, error)
	UpdateUser(email string, role string, avatar string, id string) (*auth, error)
	DeleteUser(id string) (bool, error)
}
