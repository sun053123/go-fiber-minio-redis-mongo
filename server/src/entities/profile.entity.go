package entities

import (
	"fmt"

	"go.mongodb.org/mongo-driver/mongo"
)

type profileEntity struct {
	db *mongo.Client
}

func NewProfileEntity(db *mongo.Client) ProfileEntity {
	return profileEntity{db: db}
}

func (ent profileEntity) CreateProfile(user string, firstName string, lastName string, address string, phone string) (*profile, error) {
	fmt.Println(
		"CreateProfile",
		user,
		firstName,
		lastName,
		address,
		phone,
	)
	return nil, nil
}

func (ent profileEntity) UpdateProfile(firstName string, lastName string, address string, phone string, id string) (*profile, error) {
	return nil, nil
}

func (ent profileEntity) GetProfileByUserID(userID string) (*profile, error) {
	return nil, nil
}

func (ent profileEntity) GetProfileByID(id string) (*profile, error) {
	return nil, nil
}
