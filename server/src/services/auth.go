package services

type AuthResponse struct {
	ID         string `json:"id"`
	Email      string `json:"email"`
	Role       string `json:"role"`
	Avatar     string `json:"avatar"`
	CreatedAt  string `json:"created_at"`
	UpdatedAt  string `json:"updated_at"`
	IsAccepted bool   `json:"is_accepted"`
}

type AuthRequest struct {
	Email    string `json:"email,omitempty" validate:"required,email"`
	Password string `json:"password,omitempty" validate:"required,min=6,max=50"`
	Role     string `json:"role,omitempty" validate:"required"`
	Avatar   string `json:"image,omitempty" validate:""`
	*ProfileRequest
}

type LoginRequest struct {
	Email    string `json:"email,omitempty" validate:"required,email"`
	Password string `json:"password,omitempty" validate:"required,min=6,max=50"`
}

type AuthService interface {
	GetMe(id string) (*AuthResponse, error)
	Register(AuthRequest) (string, error)
	LoginUser(LoginRequest) (string, error)
	UpdateUser(AuthRequest, id string) (*AuthResponse, error)
	DeleteUser(id string) (bool, error)
}
