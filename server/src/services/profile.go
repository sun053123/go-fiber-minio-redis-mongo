package services

import "go.mongodb.org/mongo-driver/bson/primitive"

type ProfileResponse struct {
	ID     primitive.ObjectID `json:"id"`
	UserID primitive.ObjectID `json:"user"`
	User   *AuthResponse      `json:",inline,omitempty"`
	//for find user's product (populate from db)
	Product   []*ProductResponse `json:",omitempty"`
	FirstName string             `json:"first_name"`
	LastName  string             `json:"last_name"`
	Address   string             `json:"address"`
	Phone     string             `json:"phone"`
	CreatedAt string             `json:"created_at"`
	UpdatedAt string             `json:"updated_at"`
}

type ProfileRequest struct {
	FirstName string `json:"first_name,omitempty"`
	LastName  string `json:"last_name,omitempty"`
	Address   string `json:"address,omitempty"`
	Phone     string `json:"phone,omitempty"`
}

type ProfileService interface {
	UpdateProfile() (*ProfileResponse, error)
}
