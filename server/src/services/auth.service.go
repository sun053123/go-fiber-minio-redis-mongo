package services

import (
	"fmt"
	"log"
	"os"

	"github.com/sun053123/go-paginate/errs"
	"github.com/sun053123/go-paginate/src/entities"
	"github.com/sun053123/go-paginate/utils"
	"golang.org/x/crypto/bcrypt"
)

type authService struct {
	authEnt    entities.AuthEntity
	profileEnt entities.ProfileEntity
}

func NewAuthService(authEnt entities.AuthEntity, profileEnt entities.ProfileEntity) AuthService {
	return authService{
		authEnt:    authEnt,
		profileEnt: profileEnt,
	}
}

func (serv authService) GetMe(id string) (*AuthResponse, error) {

	authDb, err := serv.authEnt.GetUserInfo(id)
	if err != nil {
		return nil, errs.NewUnexpectedError()
	}

	return &AuthResponse{
		ID:         authDb.ID.Hex(),
		Email:      authDb.Email,
		Role:       authDb.Role,
		Avatar:     authDb.Avatar,
		CreatedAt:  authDb.CreatedAt,
		UpdatedAt:  authDb.UpdatedAt,
		IsAccepted: authDb.IsAccepted,
	}, nil
}

func (serv authService) Register(authreq AuthRequest) (string, error) {

	//check if email is already exist
	//if exist return error
	email, err := serv.authEnt.GetUserInfoByEmail(authreq.Email)
	if err == nil && email != nil {
		return "", errs.NewBadRequestError("email already exist")
	}

	//validate Role
	if authreq.Role != "admin" && authreq.Role != "user" {
		return "", errs.NewBadRequestError("role must be admin or user")
	}
	//set role if admin =5101 or user =3102
	if authreq.Role == "admin" {
		authreq.Role = os.Getenv("ROLE_ADMIN")
	} else {
		authreq.Role = os.Getenv("ROLE_USER")
	}

	//bcrypt password
	passwordBytes, err := bcrypt.GenerateFromPassword([]byte(authreq.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Println(err)
		return "", errs.NewUnexpectedError()
	}
	authreq.Password = string(passwordBytes)

	//create user
	authDb, err := serv.authEnt.CreateUser(authreq.Email, authreq.Password, authreq.Role, authreq.Avatar)
	if err != nil {
		log.Println(err)
		return "", errs.NewUnexpectedError()
	}

	//TODO: create profile

	//destructor ProfileRequest from AuthRequest
	newProfile := ProfileRequest{
		FirstName: authreq.FirstName,
		LastName:  authreq.LastName,
		Phone:     authreq.Phone,
		Address:   authreq.Address,
	}

	fmt.Println("srv", newProfile)

	//create profile
	_, err = serv.profileEnt.CreateProfile(newProfile.FirstName, newProfile.LastName, newProfile.Phone, newProfile.Address, authDb.ID.Hex())
	if err != nil {
		fmt.Println(err)
	}

	//comform Claims struct
	claims := utils.Claims{
		ID:     authDb.ID.Hex(),
		Email:  authDb.Email,
		Role:   authDb.Role,
		Avatar: authDb.Avatar,
	}

	//generate token
	tokenString, err := utils.GenerateToken(claims)
	if err != nil {
		return "", errs.NewUnexpectedError()
	}
	return tokenString, nil
}

func (serv authService) LoginUser(authreq LoginRequest) (string, error) {

	//check email
	user, err := serv.authEnt.GetUserInfoByEmail(authreq.Email)
	if err != nil {
		return "", errs.NewBadRequestError("invalid credentials")
	}

	//check password
	err = bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(authreq.Password))
	if err != nil {
		return "", errs.NewBadRequestError("invalid credentials")
	}

	//comform Claims struct
	claims := utils.Claims{
		ID:     user.ID.Hex(),
		Email:  user.Email,
		Role:   user.Role,
		Avatar: user.Avatar,
	}

	//generate token
	tokenString, err := utils.GenerateToken(claims)
	if err != nil {
		return "", errs.NewUnexpectedError()
	}

	return tokenString, nil
}

func (serv authService) UpdateUser(AuthRequest, id string) (*AuthResponse, error) {

	return nil, nil
}

func (serv authService) DeleteUser(id string) (bool, error) {
	return false, nil
}
