package services

import (
	"github.com/sun053123/go-paginate/src/entities"
)

type profileService struct {
	profileEnt entities.ProfileEntity
}

func NewProfileService(profileEnt entities.ProfileEntity) ProfileService {
	return profileService{
		profileEnt: profileEnt,
	}
}

func (serv profileService) UpdateProfile() (*ProfileResponse, error) {
	return nil, nil
}
