package services

import (
	"github.com/sun053123/go-paginate/src/entities"
	"github.com/sun053123/go-paginate/utils"
)

//serializer
type ProductResponse struct {
	ID          string `json:"id"`
	UserID      string `json:"user,omitempty"`
	User        *User  `json:"creator,omitempty"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Price       int    `json:"price"`
	Quantity    int    `json:"quantity"`
	Category    string `json:"category"`
	Image       string `json:"image"`
	CreatedAt   string `json:"created_at"`
	UpdatedAt   string `json:"updated_at"`
}

type User struct {
	ID    string `json:"id,omitempty"`
	Email string `json:"email,omitempty"`
}

type DataResponse struct {
	ProductResponse []ProductResponse  `json:"product_response"`
	Pagination      *entities.Paginate `json:"pagination"`
}

type NewProductRequest struct {
	Name        string `json:"name,omitempty" validate:"min=3,max=50"`
	Description string `json:"description,omitempty" validate:"min=3,max=100"`
	Price       int    `json:"price,omitempty" validate:"min=1,max=100"`
	Quantity    int    `json:"quantity,omitempty" validate:"required,min=1,max=100"`
	Category    string `json:"category,omitempty" validate:"required"`
	Image       string `json:"image,omitempty" validate:"omitempty"`
}

type ProductService interface {
	GetAllProducts(category string, page int64) (*DataResponse, error)
	GetProductByID(id string) (*ProductResponse, error)
	GetProductByName(name string, category string, page int64) (*DataResponse, error)
	CreateProduct(newProduct NewProductRequest, userinfo utils.Claims) (*ProductResponse, error)
	UpdateProduct(newProduct NewProductRequest, userinfo utils.Claims, id string) (*ProductResponse, error)
	DeleteProduct(userinfo utils.Claims, id string) error
	SearchProduct(search string) ([]*ProductResponse, error)
	BuyProduct(id string, quantity int) (*ProductResponse, error)
}
