package services

import (
	"log"

	"github.com/sun053123/go-paginate/errs"
	"github.com/sun053123/go-paginate/src/entities"
	"github.com/sun053123/go-paginate/utils"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type productService struct {
	productEnt entities.ProductEntity
}

func covertIDToString(id primitive.ObjectID) string {
	return id.Hex()
}

func NewProductService(productEnt entities.ProductEntity) ProductService {
	return productService{
		productEnt: productEnt,
	}
}

func (serv productService) GetAllProducts(category string, page int64) (*DataResponse, error) {

	productsDB, paginationDB, err := serv.productEnt.FindAllProducts(category, page)
	if err != nil {
		return nil, errs.NewNotFoundError("product not found")
	}

	products := make([]ProductResponse, len(productsDB))
	for i, productDB := range productsDB {
		products[i] = ProductResponse{
			ID:          covertIDToString(productDB.ID),
			UserID:      covertIDToString(productDB.UserID),
			Name:        productDB.Name,
			Description: productDB.Description,
			Price:       productDB.Price,
			Quantity:    productDB.Quantity,
			Category:    productDB.Category,
			Image:       productDB.Image,
			CreatedAt:   productDB.CreatedAt,
			UpdatedAt:   productDB.UpdatedAt,
		}
	}

	return &DataResponse{
		ProductResponse: products,
		Pagination:      paginationDB,
	}, nil
}

func (serv productService) GetProductByID(id string) (*ProductResponse, error) {
	productDB, err := serv.productEnt.FindProductByID(id)
	if err != nil || productDB == nil {
		return nil, errs.NewNotFoundError("product not found")
	}

	product := ProductResponse{
		ID:          covertIDToString(productDB.ID),
		Name:        productDB.Name,
		Description: productDB.Description,
		Price:       productDB.Price,
		Quantity:    productDB.Quantity,
		Category:    productDB.Category,
		Image:       productDB.Image,
		CreatedAt:   productDB.CreatedAt,
		UpdatedAt:   productDB.UpdatedAt,
	}

	product.User = &User{
		ID:    covertIDToString(productDB.User.ID),
		Email: productDB.User.Email,
	}

	return &product, nil
}

func (serv productService) GetProductByName(name string, category string, page int64) (*DataResponse, error) {

	productsDB, paginationDB, err := serv.productEnt.FindProductByName(name, category, page)
	if err != nil {
		return nil, errs.NewNotFoundError("product not found")
	}

	//make product and user response
	products := make([]ProductResponse, len(productsDB))
	for i, productDB := range productsDB {
		products[i] = ProductResponse{
			ID:          covertIDToString(productDB.ID),
			Name:        productDB.Name,
			Description: productDB.Description,
			Price:       productDB.Price,
			Quantity:    productDB.Quantity,
			Category:    productDB.Category,
			Image:       productDB.Image,
			CreatedAt:   productDB.CreatedAt,
			UpdatedAt:   productDB.UpdatedAt,
		}
		products[i].User = &User{
			ID:    covertIDToString(productDB.User.ID),
			Email: productDB.User.Email,
		}
	}

	return &DataResponse{
		ProductResponse: products,
		Pagination:      paginationDB,
	}, nil
}

func (serv productService) CreateProduct(newProduct NewProductRequest, userinfo utils.Claims) (*ProductResponse, error) {

	//get values from createproduct interface (destructure)
	name := newProduct.Name
	description := newProduct.Description
	price := newProduct.Price
	quantity := newProduct.Quantity
	category := newProduct.Category
	imagepath := newProduct.Image

	productDB, err := serv.productEnt.CreateProduct(userinfo.ID, userinfo.Email, name, description, price, quantity, category, imagepath)
	if err != nil {
		return nil, err
	}

	product := ProductResponse{
		ID:          covertIDToString(productDB.ID),
		UserID:      covertIDToString(productDB.UserID),
		Name:        productDB.Name,
		Description: productDB.Description,
		Price:       productDB.Price,
		Quantity:    productDB.Quantity,
		Category:    productDB.Category,
		Image:       productDB.Image,
		CreatedAt:   productDB.CreatedAt,
		UpdatedAt:   productDB.UpdatedAt,
	}

	return &product, nil
}

func (serv productService) UpdateProduct(updateProduct NewProductRequest, userinfo utils.Claims, id string) (*ProductResponse, error) {

	//make chanel product is exist
	isExist := make(chan bool, 1)
	//make chanel user is creator
	isCreator := make(chan bool, 1)

	//check product is Exist and user is creator of product
	go serv.checkUserisCreator(isExist, isCreator, id, userinfo.ID)

	//wait for channel
	exist := <-isExist
	creator := <-isCreator

	if !exist {
		return nil, errs.NewNotFoundError("product not found")
	}

	if !creator {
		return nil, errs.NewUnauthorizedError("you are not creator of product")
	}

	//get values from createproduct interface (destructure)
	name := updateProduct.Name
	description := updateProduct.Description
	price := updateProduct.Price
	quantity := updateProduct.Quantity
	category := updateProduct.Category
	imagepath := updateProduct.Image

	productDB, err := serv.productEnt.UpdateProduct(name, description, price, quantity, category, imagepath, id)
	if err != nil {
		return nil, errs.NewUnexpectedError()
	}

	product := ProductResponse{
		ID:          covertIDToString(productDB.ID),
		UserID:      covertIDToString(productDB.UserID),
		Name:        productDB.Name,
		Description: productDB.Description,
		Price:       productDB.Price,
		Quantity:    productDB.Quantity,
		Category:    productDB.Category,
		Image:       productDB.Image,
		CreatedAt:   productDB.CreatedAt,
		UpdatedAt:   productDB.UpdatedAt,
	}

	return &product, nil
}

func (serv productService) DeleteProduct(userinfo utils.Claims, id string) error {

	//make chanel product is exist
	isExist := make(chan bool, 1)
	//make chanel user is creator
	isCreator := make(chan bool, 1)

	//check product is Exist and user is creator of product
	go serv.checkUserisCreator(isExist, isCreator, id, userinfo.ID)

	//wait for channel
	exist := <-isExist
	creator := <-isCreator

	if !exist {
		return errs.NewNotFoundError("product not found")
	}

	if !creator {
		return errs.NewUnauthorizedError("you are not creator of product")
	}

	updatedProduct, err := serv.productEnt.DeleteProduct(id)
	if err != nil {
		log.Println(err)
		return errs.NewUnexpectedError()
	}

	if !updatedProduct {
		return errs.NewNotFoundError("product not found")
	}

	return nil
}

func (serv productService) SearchProduct(search string) ([]*ProductResponse, error) {

	productsDB, err := serv.productEnt.SearchProduct(search)
	if err != nil {
		return nil, errs.NewNotFoundError("product not found")
	}

	products := make([]*ProductResponse, len(productsDB))
	for i, productDB := range productsDB {
		products[i] = &ProductResponse{
			ID:          covertIDToString(productDB.ID),
			Name:        productDB.Name,
			Description: productDB.Description,
			Price:       productDB.Price,
			Quantity:    productDB.Quantity,
			Category:    productDB.Category,
			Image:       productDB.Image,
			CreatedAt:   productDB.CreatedAt,
			UpdatedAt:   productDB.UpdatedAt,
		}
	}

	return products, nil
}

func (serv productService) BuyProduct(string, int) (*ProductResponse, error) {
	return nil, nil
}

// func (serv productService) checkProductisExist(isExist chan bool, id string) {
// 	//check product is exist
// 	_, err := serv.productEnt.FindProductByID(id)
// 	if err != nil {
// 		isExist <- false
// 	} else {
// 		isExist <- true
// 	}
// 	close(isExist)
// }

func (serv productService) checkUserisCreator(isExist chan bool, isCreator chan bool, id string, user string) {
	//check product is exist
	product, err := serv.productEnt.FindProductByID(id)
	if err != nil || product == nil {
		isExist <- false
	} else {
		isExist <- true
	}
	close(isExist)

	//check if user is creator of product
	if product != nil && product.User.ID.Hex() == user {
		isCreator <- true
	} else {
		isCreator <- false
	}
	close(isCreator)

}
