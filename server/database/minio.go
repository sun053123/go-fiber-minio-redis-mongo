package database

import (
	"os"

	"github.com/minio/minio-go/v7"
	"github.com/minio/minio-go/v7/pkg/credentials"
)

func GetMinioClient() (*minio.Client, error) {

	host := os.Getenv("MINIO_HOST")
	accesskey := os.Getenv("MINIO_ACCESS_KEY")
	secretkey := os.Getenv("MINIO_SECRET_KEY")

	client, err := minio.New(host, &minio.Options{
		Creds:  credentials.NewStaticV4(accesskey, secretkey, ""),
		Secure: false,
	})

	if err != nil {
		return nil, err
	}

	return client, nil
}
