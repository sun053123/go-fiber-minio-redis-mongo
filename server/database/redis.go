package database

import (
	"fmt"
	"os"

	"github.com/go-redis/redis/v8"
)

func GetRedisClient() *redis.Client {
	host := os.Getenv("REDIS_HOST")
	port := os.Getenv("REDIS_PORT")

	client := redis.NewClient(&redis.Options{
		Addr: fmt.Sprintf("%s:%s", host, port),
	})

	fmt.Println("Redis ===> Connected to Redis")

	return client
}
