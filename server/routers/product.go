package routers

import (
	"github.com/go-redis/redis/v8"
	"github.com/gofiber/fiber/v2"
	"github.com/minio/minio-go/v7"
	"github.com/sun053123/go-paginate/routers/middlewares"
	"github.com/sun053123/go-paginate/src/entities"
	"github.com/sun053123/go-paginate/src/handlers"
	"github.com/sun053123/go-paginate/src/services"
	"go.mongodb.org/mongo-driver/mongo"
)

func ProductRoutes(app *fiber.App, db *mongo.Client, redisClient *redis.Client, minioClient *minio.Client) {

	productEntity := entities.NewProductEntity(db, redisClient, minioClient)
	productService := services.NewProductService(productEntity)
	productHandler := handlers.NewProductHandler(productService)

	imageMiddleware := middlewares.NewImageMiddleware(minioClient)
	validateId := middlewares.ValidateHexIdMongo
	validateAuth := middlewares.ValidateAuth
	validateAdmin := middlewares.ValidateAuthAndAdmin

	api := app.Group("/api/v1")
	api.Get("/products", productHandler.GetAllProducts)
	api.Get("/products/search", productHandler.SearchProduct)
	api.Get("/products/:id", validateAuth, validateId, productHandler.GetProductByID)
	api.Post("/products", validateAdmin, imageMiddleware.StoreImage, productHandler.CreateProduct)
	api.Delete("/products/:id", validateAdmin, validateId, productHandler.DeleteProduct)
	api.Patch("/products/:id", validateAdmin, validateId, imageMiddleware.StoreImage, productHandler.UpdateProduct)

}
