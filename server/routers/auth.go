package routers

import (
	"github.com/gofiber/fiber/v2"
	"github.com/sun053123/go-paginate/routers/middlewares"
	"github.com/sun053123/go-paginate/src/entities"
	"github.com/sun053123/go-paginate/src/services"
	"go.mongodb.org/mongo-driver/mongo"

	"github.com/sun053123/go-paginate/src/handlers"
)

func AuthRoutes(app *fiber.App, db *mongo.Client) {

	// dependency injection for handlers
	profileEnt := entities.NewProfileEntity(db)
	authEntity := entities.NewAuthEntity(db)
	authService := services.NewAuthService(authEntity, profileEnt)
	authHandler := handlers.NewAuthHandler(authService)

	// middlewares
	validateAuth := middlewares.ValidateAuth

	app.Get("/api/v1/auth/me", validateAuth, authHandler.GetMe)
	app.Post("/api/v1/auth/login", authHandler.Login)
	app.Post("/api/v1/auth/register", authHandler.Register)
	app.Get("/api/v1/auth/logout", authHandler.Logout)
}
