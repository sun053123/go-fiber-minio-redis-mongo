package middlewares

import "github.com/gofiber/fiber/v2"

type ImageStoreMiddleware interface {
	StoreImage(c *fiber.Ctx) error
}
