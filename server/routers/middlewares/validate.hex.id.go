package middlewares

import (
	"net/http"

	"github.com/gofiber/fiber/v2"
	"github.com/sun053123/go-paginate/errs"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func ValidateHexIdMongo(c *fiber.Ctx) error {
	id := c.Params("id")

	if isHex(id) { // if not valid hex id
		return c.Status(http.StatusBadRequest).JSON(errs.AppError{
			Message: "Invalid ID",
			Code:    http.StatusBadRequest,
		})
	}

	return c.Next()
}

func isHex(id string) bool {
	_, err := primitive.ObjectIDFromHex(id)

	return err != nil
}
