package middlewares

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"github.com/minio/minio-go/v7"
	"github.com/sun053123/go-paginate/errs"
	"github.com/sun053123/go-paginate/utils"
)

type minioStorage struct {
	minio *minio.Client
}

func NewImageMiddleware(minio *minio.Client) ImageStoreMiddleware {
	return minioStorage{
		minio: minio,
	}
}

func (m minioStorage) StoreImage(c *fiber.Ctx) error {

	ctx := context.Background()
	bucketName := os.Getenv("MINIO_BUCKET_NAME")
	region := os.Getenv("MINIO_REGION")

	//get image from request
	file, err := c.FormFile("image")
	if file == nil {
		return c.Next()
	}
	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(errs.AppError{
			Message: "Unexpected error",
			Code:    http.StatusInternalServerError,
		})
	}

	//check if not jpg jpeg png return error
	if !utils.IsImage(file) {
		return c.Status(http.StatusBadRequest).JSON(errs.AppError{
			Message: "Invalid image type",
			Code:    http.StatusBadRequest,
		})
	}

	//check if we have file from request
	if file == nil || file.Size == 0 || file.Filename == "" {
		return c.Next()
	}

	//generate unique file name
	uniqueId := uuid.New()

	//remove - from img path
	fileName := strings.Replace(uniqueId.String(), "-", "", -1)

	//extract file extension
	fileExt := strings.Split(file.Filename, ".")[1]

	imageName := fmt.Sprintf("%s.%s", fileName, fileExt)

	makepathImg := fmt.Sprintf("http://%s/%s/%s", os.Getenv("MINIO_HOST"), os.Getenv("MINIO_BUCKET_NAME"), imageName)

	// uploadChanel := make(chan minio.UploadInfo)
	// errUploadChanel := make(chan error)

	//TODO: Resize Image Before save into database

	//resize image and get buffer from file (if image greater than 1MB)
	//switch case if image size greater than 1MB

	// Concurrcy RESIZE AND UPLOAD.
	go func() error {

		//make buffer dynamic type
		var fileSize int64
		var buffer *bytes.Buffer
		if file.Size > 1000000 {
			buffer, err = utils.ResizeImage(file, 100, 100)
			if err != nil {
				log.Printf("image resize error --> %s", err)
				return err
			}

			fileSize = int64(buffer.Len())
		} else {

			// if no need to resize image
			bufferM, err := file.Open()
			if err != nil {
				return c.Status(http.StatusInternalServerError).JSON(errs.AppError{
					Message: "Unexpected error",
					Code:    http.StatusInternalServerError,
				})
			}
			defer bufferM.Close()

			// multipart file to byte buffer
			buffer = &bytes.Buffer{}
			_, err = io.Copy(buffer, bufferM)
			if err != nil {
				return err
			}
			fileSize = file.Size

		}

		//save to images folder (not in use)
		// err = c.SaveFile(file, fmt.Sprintf("./images/%s", image))
		// if err != nil {
		// 	log.Printf("image save error --> %s", err)
		// 	return c.JSON(fiber.Map{"status": 500, "message": "Server error", "data": nil})
		// }

		//create bucket
		errMakeBucket := m.minio.MakeBucket(ctx, bucketName, minio.MakeBucketOptions{
			Region: region,
		})
		if errMakeBucket != nil {
			// Check to see if we already own this bucket (which happens if you run this twice)
			exists, errBucketExist := m.minio.BucketExists(ctx, bucketName)
			if errBucketExist != nil {
				log.Printf("check bucket exist error")
				return err
			}
			if !exists {
				log.Printf("make bucket error")
				return err
			}
		} else {
			log.Printf("Successfully created %s\n", bucketName)
		}

		//save to minio
		contentType := file.Header["Content-Type"][0]

		// Upload the file to the bucket
		uploadinfo, errupload := m.minio.PutObject(ctx, bucketName, imageName, buffer, fileSize, minio.PutObjectOptions{ContentType: contentType})
		if errupload != nil {
			log.Printf("upload file error")
			return err
		}

		// uploadChanel <- uploadinfo
		// errUploadChanel <- errupload
		_ = uploadinfo

		//create cdn image path (exp: https://cdn.example.com/prodcuts/image.jpg)
		// makepathImg := fmt.Sprintf("http://%s/%s/%s", os.Getenv("MINIO_HOST"), uploadinfo.Bucket, uploadinfo.Key)

		return nil
	}()

	// get upload info from channel it took too long to get from channel (1.6 mb = 400ms = no go routine)
	// uploadedInfo := <-uploadChanel
	// errUpload := <-errUploadChanel
	// if errUpload != nil {
	// 	return c.Status(http.StatusBadRequest).JSON(fiber.Map{
	// 		"status":  http.StatusBadRequest,
	// 		"message": "Server error",
	// 	})
	// }

	// fmt.Println(uploadedInfo)

	//upload to local variable and use in handler
	c.Locals("uploadedImage", makepathImg)

	return c.Next()

	// if uploadedInfo.Key != "" {
	// 	return c.Next()
	// }

	// return c.Status(http.StatusBadRequest).JSON(fiber.Map{
	// 	"status":  http.StatusBadRequest,
	// 	"message": "Server error, cannot upload image",
	// })

}
