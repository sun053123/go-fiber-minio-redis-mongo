package middlewares

import (
	"log"
	"net/http"
	"os"

	"github.com/gofiber/fiber/v2"
	"github.com/sun053123/go-paginate/errs"
	"github.com/sun053123/go-paginate/utils"
)

func ValidateAuth(c *fiber.Ctx) error {

	cookie := c.Cookies("jwt-token")

	claims, err := utils.VerifyToken(cookie)
	if err != nil {
		return c.Status(http.StatusUnauthorized).JSON(errs.NewUnauthorizedError("Invalid token"))
	}

	//if token is not found
	if claims == nil {
		log.Println("token is not found")
		return c.Status(http.StatusUnauthorized).JSON(errs.NewUnauthorizedError("Invalid token"))
	}

	//claims to locals
	c.Locals("userinfo", claims)

	return c.Next()
}

func ValidateAuthAndAdmin(c *fiber.Ctx) error {

	cookie := c.Cookies("jwt-token")

	claims, err := utils.VerifyToken(cookie)
	if err != nil {
		return c.Status(http.StatusUnauthorized).JSON(errs.NewUnauthorizedError("Invalid token"))
	}

	//if token is not found
	if claims == nil {
		return c.Status(http.StatusUnauthorized).JSON(errs.NewUnauthorizedError("Invalid token"))
	}

	//if user is not admin
	if claims.Role != os.Getenv("ROLE_ADMIN") {
		log.Println("user is not admin", claims.Role)
		return c.Status(http.StatusForbidden).JSON(errs.AppError{
			Message: "sorry, you are not admin",
			Code:    http.StatusForbidden,
		})
	}

	//claims to locals
	c.Locals("userinfo", claims)

	return c.Next()

}
