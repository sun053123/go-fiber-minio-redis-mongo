package utils

import (
	"bytes"
	"fmt"
	"image"
	"image/jpeg"
	"image/png"
	"mime/multipart"

	"github.com/pkg/errors"
	"golang.org/x/image/draw"
)

func ResizeImage(file *multipart.FileHeader, width int, height int) (*bytes.Buffer, error) {

	//decode file *multipart.FileHeader to image.Image
	buffer, err := file.Open()
	if err != nil {
		fmt.Println("1")
		return nil, err
	}
	defer buffer.Close()

	var img image.Image
	var errDecode error

	switch file.Header.Get("Content-Type") {
	case "image/jpeg":
		img, errDecode = jpeg.Decode(buffer)

	case "image/png":
		img, errDecode = png.Decode(buffer)
	}

	if errDecode != nil {
		return nil, errors.Wrap(err, "unable to decode img")
	}

	//use NearestNeighbor resampling algorithm to resize image down
	dst := image.NewRGBA(image.Rect(0, 0, img.Bounds().Max.X/5, img.Bounds().Max.Y/5))

	draw.NearestNeighbor.Scale(dst, dst.Rect, img, img.Bounds(), draw.Over, nil)

	//create buffer
	buf := new(bytes.Buffer)

	//save resized image to buffer
	png.Encode(buf, dst)

	return buf, nil
}

func IsImage(file *multipart.FileHeader) bool {
	return file.Header.Get("Content-Type") == "image/jpeg" || file.Header.Get("Content-Type") == "image/png"
}
