package utils

import (
	"fmt"
	"os"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/gofiber/fiber/v2"
)

type Claims struct {
	ID     string `json:"id"`
	Email  string `json:"email"`
	Role   string `json:"role"`
	Avatar string `json:"avatar"`
	jwt.StandardClaims
}

func GenerateToken(userinfo Claims) (tokenString string, err error) {
	// set userinfo to claims
	cliams := &Claims{
		ID:     userinfo.ID,
		Email:  userinfo.Email,
		Role:   userinfo.Role,
		Avatar: userinfo.Avatar,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: time.Now().Add(time.Hour * 72).Unix(),
			Issuer:    userinfo.ID,
		},
	}
	// Create the token sign with algo HS256
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, cliams)
	
	// Sign the token with our secret
	tokenString, err = token.SignedString([]byte(os.Getenv("JWT_SECRET")))
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func VerifyToken(cookie string) (claims *Claims, err error) {

	//get token from cookie
	token, err := jwt.ParseWithClaims(cookie, &Claims{}, func(token *jwt.Token) (interface{}, error) {
		return []byte(os.Getenv("JWT_SECRET")), nil
	})

	if err != nil {
		return nil, err
	}

	//if token is not found
	if token == nil {
		return nil, fiber.ErrUnauthorized
	}

	vclaims := token.Claims.(*Claims)
	claims = &Claims{
		ID:     vclaims.ID,
		Email:  vclaims.Email,
		Role:   vclaims.Role,
		Avatar: vclaims.Avatar,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: vclaims.ExpiresAt,
			Issuer:    vclaims.Issuer,
		},
	}

	//check if token is expired
	if time.Unix(claims.StandardClaims.ExpiresAt, 0).Before(time.Now()) {
		return nil, fmt.Errorf("token expired")
	}

	return claims, nil
}
